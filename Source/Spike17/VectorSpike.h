// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Kismet/BlueprintFunctionLibrary.h"
#include "VectorSpike.generated.h"

/**
 * 
 */
UCLASS()
class SPIKE17_API UVectorSpike : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()

public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "VectorSpike")
		float x;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "VectorSpike")
		float y;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "VectorSpike")
		float z;

	UVectorSpike();

	UFUNCTION(BlueprintCallable, Category = "VectorMethod")
	UVectorSpike* vectorAddition(UVectorSpike* vectorA, UVectorSpike* vectorB);

	UFUNCTION(BlueprintCallable, Category = "VectorMethod")
	UVectorSpike* vectorSubtraction(UVectorSpike* vectorA, UVectorSpike* vectorB);

	UFUNCTION(BlueprintCallable, Category = "VectorMethod")
	UVectorSpike* vectorMultiplication(UVectorSpike* vectorA, UVectorSpike* vectorB);

	UFUNCTION(BlueprintCallable, Category = "VectorMethod")
	UVectorSpike* vectorDivision(UVectorSpike* vectorA, UVectorSpike* vectorB);

	UFUNCTION(BlueprintCallable, Category = "VectorMethod")
	float vectorDotProduct(UVectorSpike* vectorA, UVectorSpike* vectorB, float cosTheta);
	
	
	
};
