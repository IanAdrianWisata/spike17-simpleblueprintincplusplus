// Fill out your copyright notice in the Description page of Project Settings.

#include "Spike17.h"
#include "VectorSpike.h"

UVectorSpike::UVectorSpike()
{
	x = 0;
	y = 0;
	z = 0;
}
UVectorSpike * UVectorSpike::vectorAddition(UVectorSpike * vectorA, UVectorSpike * vectorB)
{
	UVectorSpike* result = NewObject<UVectorSpike>();

	result->x = vectorA->x + vectorB->x;
	result->y = vectorA->y + vectorB->y;
	result->z = vectorA->z + vectorB->z;

	return result;
}

UVectorSpike * UVectorSpike::vectorSubtraction(UVectorSpike * vectorA, UVectorSpike * vectorB)
{
	UVectorSpike* result = NewObject<UVectorSpike>();

	result->x = vectorA->x - vectorB->x;
	result->y = vectorA->y - vectorB->y;
	result->z = vectorA->z - vectorB->z;

	return result;
}

UVectorSpike * UVectorSpike::vectorMultiplication(UVectorSpike * vectorA, UVectorSpike * vectorB)
{
	UVectorSpike* result = NewObject<UVectorSpike>();

	result->x = vectorA->x * vectorB->x;
	result->y = vectorA->y * vectorB->y;
	result->z = vectorA->z * vectorB->z;

	return result;
}

UVectorSpike * UVectorSpike::vectorDivision(UVectorSpike * vectorA, UVectorSpike * vectorB)
{
	UVectorSpike* result = NewObject<UVectorSpike>();

	result->x = vectorA->x / vectorB->x;
	result->y = vectorA->y / vectorB->y;
	result->z = vectorA->z / vectorB->z;

	return result;
}

float UVectorSpike::vectorDotProduct(UVectorSpike * vectorA, UVectorSpike * vectorB, float cosTheta)
{
	float result;

	result = (vectorA->x * vectorB->x) + (vectorA->y * vectorB->y) + (vectorA->z * vectorB->z) * cosTheta;

	return result;
}
